# Дано число 1 <= n <= 10^7, необходимо найти последнюю цифру n-го числа Фибоначчи

def fib(n):
    f_m = [0, 1]
    for y in range(2, n + 1):
        f_m.append((f_m[y-1] + f_m[y-2]) % 10)
    return f_m[n]


def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()
