# По данным двум числам 1 <= n <= 10^18 и 2 <= m <= 10^5, необходимо найти остаток
# от деления nn-го числа Фибоначчи на mm.

def fib_mod(n, m):
    p = [0, 1]
    x, y = 0, 1
    for i in range(6 * m):
        x, y = y, (x+y) % m
        p.append(y % m)
        if p[-1] == 1 and p[-2] == 0:
            break
    return p[n % (len(p) - 2)]


def main():
    n, m = map(int, input().split())
    print(fib_mod(n, m))


if __name__ == "__main__":
    main()
