#   Дано целое число 1 <= n <= 40, необходимо вычислить n-е число Фибоначчи

def fib(n):
    f_m = [0, 1]
    for y in range(2, n + 1):
        f_m.append(f_m[y-1] + f_m[y-2])
    return f_m[n]


def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()