import pytest
from pages.login_page import LoginPage
from pages.product_page import ProductPage
from pages.basket_page import BasketPage

link_product = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"


@pytest.mark.need_review
def test_guest_can_add_product_to_basket(browser):
    product_page = ProductPage(browser, link_product)
    product_page.open()
    product_page.add_product_to_basket()
    product_page.should_be_name_product()
    product_page.should_be_product_price_added_to_basket()


@pytest.mark.xfail
def test_guest_cant_see_success_message_after_adding_product_to_basket(browser):
    product_page = ProductPage(browser, link_product)
    product_page.open()
    product_page.add_product_to_basket()
    product_page.should_not_be_success_message()


def test_guest_cant_see_success_message(browser):
    product_page = ProductPage(browser, link_product)
    product_page.open()
    product_page.should_not_be_success_message()


@pytest.mark.xfail
def test_message_disappeared_after_adding_product_to_basket(browser):
    product_page = ProductPage(browser, link_product)
    product_page.open()
    product_page.add_product_to_basket()
    product_page.should_not_is_disappeared()


def test_guest_should_see_login_link_on_product_page(browser):
    page = ProductPage(browser, link_product)
    page.open()
    page.should_be_login_link()


@pytest.mark.need_review
def test_guest_can_go_to_login_page_from_product_page(browser):
    page = ProductPage(browser, link_product)
    page.open()
    page.go_to_login_page()


@pytest.mark.need_review
def test_guest_cant_see_product_in_basket_opened_from_product_page(browser):
    page = BasketPage(browser, link_product)
    page.open()
    page.go_to_basket_page()
    page.should_product_not_in_basket()
    page.should_message_availability_product_in_basket()


class TestUserAddToBasketFromProductPage:

    @pytest.fixture(scope="function", autouse=True)
    def setup(self, browser):
        product_page = LoginPage(browser, link_product)
        product_page.open()
        product_page.go_to_login_page()
        product_page.register_new_user()
        product_page.should_be_authorized_user()

    @pytest.mark.need_review
    def test_user_can_add_product_to_basket(self, browser):
        product_page = ProductPage(browser, link_product)
        product_page.open()
        product_page.add_product_to_basket()
        product_page.should_be_name_product()
        product_page.should_be_product_price_added_to_basket()

    def test_user_cant_see_success_message(self, browser):
        product_page = ProductPage(browser, link_product)
        product_page.open()
        product_page.should_not_be_success_message()


