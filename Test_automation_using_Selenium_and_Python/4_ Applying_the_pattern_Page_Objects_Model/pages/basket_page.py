from .locators import LoginPageLocators, BasketPageLocators
from .base_page import BasePage


class BasketPage(BasePage):

    def should_product_not_in_basket(self):
        assert self.is_not_element_present(*BasketPageLocators.MESSAGE_PRODUCT_IN_BASKET), "В корзине есть товары"
        assert True

    def should_message_availability_product_in_basket(self):
        assert self.is_element_present(*BasketPageLocators.AVAILABILITY_PRODUCT_IN_BASKET), "Нет сообщения о том. что "\
                                                                                             "корзина пуста"
        assert True
