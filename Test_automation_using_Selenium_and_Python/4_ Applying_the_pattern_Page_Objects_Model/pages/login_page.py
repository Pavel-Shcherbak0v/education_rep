import pytest
import time
from .locators import LoginPageLocators
from .base_page import BasePage

email = str(time.time()) + "@mail.ru"
password = "local21312323"


class LoginPage(BasePage):

    def should_be_login_page(self):
        self.should_be_login_url()
        self.should_be_login_form()
        self.should_be_register_form()

    def register_new_user(self):
        input_email = self.browser.find_element(*LoginPageLocators.REG_MAIL)
        email_paste = str(time.time()) + "@mail.ru"
        input_email.send_keys(email_paste)
        input_password = self.browser.find_element(*LoginPageLocators.REG_PASS)
        input_password.send_keys(password)
        input_re_password = self.browser.find_element(*LoginPageLocators.REG_RE_PASS)
        input_re_password.send_keys(password)
        button_reg = self.browser.find_element(*LoginPageLocators.BTN_REG)
        button_reg.click()

    def should_be_login_url(self):
        assert "login" in self.browser.current_url, "URL is not correct"
        assert True

    def should_be_login_form(self):
        assert self.is_element_present(*LoginPageLocators.SIGN_IN_MAIL), "Login form is not presented"
        assert True

    def should_be_register_form(self):
        assert self.is_element_present(*LoginPageLocators.REG_MAIL), "Registration form is not presented"
        assert True

    def should_be_authorized_user(self):
        assert self.is_element_present(*LoginPageLocators.USER_REG_MESSAGE), "The message about successful " \
                                                                             "authorization was not received "
        assert True
