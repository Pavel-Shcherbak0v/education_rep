from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")


class LoginPageLocators:
    SIGN_IN_MAIL = (By.CSS_SELECTOR, "#id_login-username")
    SIGN_IN_PASSWORD = (By.CSS_SELECTOR, "#id_login-password")
    REG_MAIL = (By.CSS_SELECTOR, "#id_registration-email")
    REG_PASS = (By.CSS_SELECTOR, "#id_registration-password1")
    REG_RE_PASS = (By.CSS_SELECTOR, "#id_registration-password2")
    USER_REG_MESSAGE = (By.CSS_SELECTOR, ".alert-success .alertinner.wicon")
    BTN_REG = (By.CSS_SELECTOR, "[name='registration_submit']")


class ProductPageLocators:
    BTN_ADD_TO_BASKET = (By.CSS_SELECTOR, ".btn-add-to-basket")
    PRODUCT_PRICE = (By.CSS_SELECTOR, ".col-sm-6.product_main .price_color")
    TOTAL_PRICE_TO_BASKET = (By.CSS_SELECTOR, ".alertinner p strong")
    PRODUCT_NAME = (By.CSS_SELECTOR, ".col-sm-6.product_main h1")
    ADDED_PRODUCT_NAME = (By.CSS_SELECTOR, "#messages .alertinner strong")


class BasePageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")
    LOGIN_LINK_INVALID = (By.CSS_SELECTOR, "#login_link_inc")
    BASKET_LINK = (By.CSS_SELECTOR, ".pull-right.hidden-xs .btn-group a.btn.btn-default")
    USER_ICON = (By.CSS_SELECTOR, ".icon-user")


class BasketPageLocators:
    MESSAGE_PRODUCT_IN_BASKET = (By.CSS_SELECTOR, ".col-sm-6.h3")
    AVAILABILITY_PRODUCT_IN_BASKET = (By.CSS_SELECTOR, "p:nth-child(1)")
