from .locators import ProductPageLocators
from .base_page import BasePage


class ProductPage(BasePage):

    def add_product_to_basket(self):
        btn_add_to_basket = self.browser.find_element(*ProductPageLocators.BTN_ADD_TO_BASKET)
        btn_add_to_basket.click()
        #self.solve_quiz_and_get_code()

    def should_be_name_product(self):
        added_product_name = self.browser.find_element(*ProductPageLocators.ADDED_PRODUCT_NAME)
        product_name = self.browser.find_element(*ProductPageLocators.PRODUCT_NAME)
        assert product_name.text == added_product_name.text, "Наименование добавленного товаров не совпадает с окном"
        assert True

    def should_be_product_price_added_to_basket(self):
        price_added_product = self.browser.find_element(*ProductPageLocators.PRODUCT_PRICE)
        total_price_basket = self.browser.find_element(*ProductPageLocators.TOTAL_PRICE_TO_BASKET)
        assert price_added_product.text == total_price_basket.text, "Стоимость товара не равна стоимости товаров в " \
                                                                    "корзине "
        assert True

    def should_not_be_success_message(self):
        assert self.is_not_element_present(*ProductPageLocators.ADDED_PRODUCT_NAME), \
            "Success message is presented, but should not be"
        assert True

    def should_not_is_disappeared(self):
        assert self.is_disappeared(*ProductPageLocators.ADDED_PRODUCT_NAME), \
            "Success message is presented, but should not be"
        assert True


