import pytest
from selenium import webdriver
import time
import math


@pytest.fixture(scope="function")
def browser():
    browser = webdriver.Chrome()
    browser.implicitly_wait(5)
    yield browser
    browser.quit()


@pytest.mark.parametrize('lesson', ["236895", "236896", "236897", "236898", "236899", "236903", "236904", "236905"])
def test_guest_should_see_login_link(browser, lesson):
    link = f"https://stepik.org/lesson/{lesson}/step/1/"
    browser.get(link)
    answer = str(math.log(int(time.time())))
    input1 = browser.find_element_by_css_selector(".ember-text-area")
    input1.send_keys(answer)
    button = browser.find_element_by_css_selector(".submit-submission")
    button.click()
    result = browser.find_element_by_css_selector(".smart-hints__hint")
    good_result = 'Correct!'
    assert result.text == good_result, "Good"
