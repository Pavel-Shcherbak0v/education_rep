import unittest
from selenium import webdriver


class TestUnittest(unittest.TestCase):
    def test_unittest1(self):
        link = "http://suninjuly.github.io/registration1.html"
        browser = webdriver.Chrome()
        browser.get(link)

        # Ваш код, который заполняет обязательные поля
        input1 = browser.find_element_by_xpath(
            "//input[@class='form-control first' and @placeholder='Input your first name']")
        input1.send_keys("Ivan")
        input2 = browser.find_element_by_xpath(
            "//input[@class='form-control second' and @placeholder='Input your last name']")
        input2.send_keys("Ivanov")
        input3 = browser.find_element_by_xpath(
            "//input[@class='form-control third' and @placeholder='Input your email']")
        input3.send_keys("Petrov@mail.ru")
        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button.btn")
        button.click()

        welcome_text_elt = browser.find_element_by_tag_name("h1")
        welcome_text = welcome_text_elt.text
        self.assertEqual(welcome_text, "Congratulations! You have successfully registered!",
                         "Should be absolute value of a number")

    def test_unittest(self):
        link = "http://suninjuly.github.io/registration2.html"
        browser = webdriver.Chrome()
        browser.get(link)

        # Ваш код, который заполняет обязательные поля
        input1 = browser.find_element_by_xpath(
            "//input[@class='form-control first' and @placeholder='Input your first name']")
        input1.send_keys("Ivan")
        input2 = browser.find_element_by_xpath(
            "//input[@class='form-control second' and @placeholder='Input your last name']")
        input2.send_keys("Ivanov")
        input3 = browser.find_element_by_xpath(
            "//input[@class='form-control third' and @placeholder='Input your email']")
        input3.send_keys("Petrov@mail.ru")
        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button.btn")
        button.click()

        welcome_text_elt = browser.find_element_by_tag_name("h1")
        welcome_text = welcome_text_elt.text
        self.assertEqual(welcome_text, 'Congratulations! You have successfully registered!', "Should be absolute value of a number")


if __name__ == "__main__":
    unittest.main()

