from selenium import webdriver
import time
import os

try:

    link = "http://suninjuly.github.io/file_input.html"
    browser = webdriver.Chrome()
    browser.get(link)

    input1 = browser.find_element_by_name("firstname")
    input1.send_keys("Pavel")

    input2 = browser.find_element_by_name("lastname")
    input2.send_keys("Shcherbakov")

    input3 = browser.find_element_by_name("email")
    input3.send_keys("pyuscherbakov@gmail.com")

    # находим элемент <input type="file">
    element = browser.find_element_by_id("file")

    # заполняем элемент путём до загружаемого файла
    element.send_keys(os.getcwd() + "/file.txt")

    # находим элемент <input type="file">
    element = browser.find_element_by_id("file")

    # нажимаем на элемент (отправляем форму)
    element = browser.find_element_by_tag_name("button")
    element.click()

    time.sleep(1)

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
