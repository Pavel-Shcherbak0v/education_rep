from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select

try:

    link = "http://suninjuly.github.io/selects2.html"
    browser = webdriver.Chrome()
    browser.get(link)

    a = browser.find_element_by_css_selector("#num1").text
    b = browser.find_element_by_css_selector("#num2").text
    c = int(a) + int(b)

    select = Select(browser.find_element_by_tag_name("select"))
    select.select_by_visible_text('%s' % c)

    button = browser.find_element_by_css_selector(".btn.btn-default")
    button.click()

    time.sleep(1)

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()
