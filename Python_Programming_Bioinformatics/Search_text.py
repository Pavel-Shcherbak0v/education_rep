"""
Имеется набор файлов, каждый из которых, кроме последнего, содержит имя следующего файла.
Первое слово в тексте последнего файла: "We".

Скачайте предложенный файл. В нём содержится ссылка на первый файл из этого набора.

Все файлы располагаются в каталоге по адресу:
https://stepic.org/media/attachments/course67/3.6.3/
"""

import requests
sch = 0  # Переменная счетчика итераций цикла
with open("dataset_3378_3.txt", "r+") as inf_link:
    s = requests.get("https://stepic.org/media/attachments/course67/3.6.3/699991.txt")  # Скачиваем первый текст
    while True:
        if not s.text.startswith('We'):  # Если текст, не начинается со строки "We"
            s = requests.get('https://stepic.org/media/attachments/course67/3.6.3/' + s.text)   # Считать имя нового
            # файла из полученного текста и перейти по ссылке
            sch += 1
            print("Работаю (итерация: " + str(sch) + ")")        # Выводим итерацию, что бы видеть, работает ли наш код
        else:
            print(s.text)                 # Выводим результат
            break
