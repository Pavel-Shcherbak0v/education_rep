"""
Напишите программу, которая считывает текст из файла (в файле может быть больше одной строки)
и выводит самое частое слово в этом тексте и через пробел то, сколько раз оно встретилось.
Если таких слов несколько, вывести лексикографически первое (можно использовать оператор < для строк).
"""
with open("dataset_3363_3.txt", "r+") as inf:
    s1 = inf.read().lower().split()
    d_res = {}
    result = {}
    d_2 = int(0)
    d_ch1 = ""
    for cnt in range(len(s1)-1):
        d_res[s1[cnt]] = s1.count(s1[cnt])
        d_1 = d_res.setdefault(s1[cnt])
        if d_1 > d_2:
            d_2 = d_1
            d_ch2 = s1[cnt]
            if d_ch2 > d_ch1:
                d_ch1 = d_ch2
                inf.write('\n')
                inf.write(d_ch2 + " ")
    inf.write(str(d_2))
