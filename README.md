## Учебный репозиторий
Репозиторий содержит файлы выполненных заданий в процессе обучения.

*Содержание:*
- [Программирование на Python](https://github.com/Pavel-Shcherbak0v/education_rep/tree/main/Python_Programming_Bioinformatics)
- [Автоматизация тестирования с помощью Selenium и Python](https://github.com/Pavel-Shcherbak0v/education_rep/tree/main/Test_automation_using_Selenium_and_Python)
