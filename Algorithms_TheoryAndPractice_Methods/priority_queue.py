"""
В первой строке даны целое число 1 ≤ n ≤ 10^5 и массив A[1…n] из n различных натуральных чисел, не превышающих 10^9,
в порядке возрастания, во второй — целое число 1 ≤ k ≤ 10^5 и k натуральных чисел b1, …, bk, не превышающих 10^9. 
Для каждого i от 1 до k необходимо вывести индекс 1 ≤ j ≤ n, для которого A[j] = bi, или −1.
"""

import heapq


class PriorityQueue:
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]


def main():
    n = int(input())
    pq = PriorityQueue()
    maxs = []
    for _ in range(n):
        command = input()
        if command.startswith('Insert'):
            _, priority = command.split()
            priority = int(priority)
            pq.push(priority, priority)
        else:
            maxs.append(pq.pop())
    for m in maxs:
        print(m)


if __name__ == '__main__':
    main()
