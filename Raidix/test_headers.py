import requests
import pytest
from hamcrest import *
import allure


@allure.feature('Тесты API httpbin')
@allure.story('/headers')
class TestHeaders:

    @pytest.fixture(autouse=True)
    def _setup(self):
        base_url = 'https://httpbin.org/'
        resource = '/headers'
        self.url = base_url + resource
        self.default_headers = ('Accept', 'Accept-Encoding', 'Host', 'User-Agent', 'X-Amzn-Trace-Id')

    @allure.title('Получаем успешный ответ при get-запросе')
    def test_see_200_response_on_get_request(self):
        r = requests.get(self.url)
        assert_that(r.status_code, equal_to(200))

    @allure.title('Получаем headers в теле ответа')
    def test_see_headers_in_body(self):
        r = requests.get(self.url)
        assert_that('headers', is_in(r.json()))

    @allure.title('Получаем дефолтные заголовки в теле ответа')
    def test_see_default_headers_in_response(self):
        test_headers = {}
        r = requests.get(self.url, headers=test_headers)
        assert_that(r.json()['headers'].keys(), contains_inanyorder(*self.default_headers))

    @allure.title('Получаем переданный заголовок запроса в теле ответа')
    def test_see_new_header_from_request(self):
        test_header_name = 'Test123'
        test_header_value = '123'
        test_headers = {test_header_name: test_header_value}
        r = requests.get(self.url, headers=test_headers)
        assert_that(r.json()['headers'], has_entry(test_header_name, test_header_value))

    @allure.title('Меняем значение дефолтного заголовка, если передаём его в запросе')
    def test_replace_default_header_value(self):
        test_header_name = self.default_headers[0]
        test_header_value = '123'
        test_headers = {test_header_name: test_header_value}
        r = requests.get(self.url, headers=test_headers)
        assert_that(r.json()['headers'], has_entry(test_header_name, test_header_value))

    @allure.title('Получаем ответ с 1000 заголовков')
    def test_get_1000_headers(self):
        """
        Какого-то фиксированного лимита нет, поэтому проверяем 1000 заголовков.
        """
        max_headers_count = 1000
        test_headers = {"test_name_" + str(i): str(i) for i in range(max_headers_count)}
        r = requests.get(self.url, headers=test_headers)
