import os
import subprocess
import datetime

out = os.popen('df -h').read()  # Запрашиваем данные о жестких дисках и записываем их в переменную
print(out)

data = datetime.datetime.now()  # Находим текущую дату
subprocess.run(["tar -cvzf", data.strftime("%d-%m-%Y") + ".tar.gz /home/somename/work"])  # Архивируем файлы
subprocess.run(["mv", data.strftime("%d-%m-%Y") + ".tar.gz /tmp"])  # Перемещаем архив в /tmp/
subprocess.run(["find / -type f -size +1G -exec rm {}"])  # Находим и удаляем файлы > 1 Gb
