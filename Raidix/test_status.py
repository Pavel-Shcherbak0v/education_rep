import pytest
import requests
import logging
import datetime
from hamcrest import *
import allure

base_url = "https://httpbin.org/"
resource = "/status/"

data = datetime.datetime.now()
logging.basicConfig(
    level=logging.DEBUG,
    filename="test_status " + data.strftime("%d-%m-%Y (%H:%M:%S)") + ".log",
    format="%(asctime)s - %(module)s - %(levelname)s - %(funcName)s: %(lineno)d - %(message)s",
    datefmt='%H:%M:%S',
)

status_code = [101, 102, 103, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226, 300, 301, 302, 303, 304, 305, 306,
               307, 308, 400, 401, 402, 403, 404, 405, 406, 407, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417,
               418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 449, 451, 499, 500, 501, 502, 503,
               504, 505, 506, 507, 508, 509, 510, 511, 520, 521, 522, 523, 524, 525, 526]


@pytest.mark.parametrize(
    "send", status_code)
@allure.title("Сравниваем отправленный статус код {send} GET запросом с полученным")
def test_see_status_code_with_get_request(send):  # Проверка статусов при отправке GET запроса
    url = base_url + resource + str(send)
    r = requests.get(url)
    assert_that(r.status_code, equal_to(send))


@pytest.mark.parametrize(
    "send", status_code)
@allure.title("Сравниваем отправленный статус код {send} POST запросом с полученным)")
def test_see_status_code_with_post_request(send):  # Проверка статусов при отправке POST запроса
    url = base_url + resource + str(send)
    r = requests.post(url)
    assert_that(r.status_code, equal_to(send))


@pytest.mark.parametrize(
    "send", status_code)
@allure.title("Сравниваем отправленный статус код {send} PUT запросом с полученным")
def test_see_status_code_with_put_request(send):  # Проверка статусов при отправке PUT запроса
    url = base_url + resource + str(send)
    r = requests.put(url)
    assert_that(r.status_code, equal_to(send))


@pytest.mark.parametrize(
    "send", status_code)
@allure.title("Сравниваем отправленный статус код {send} PATCH запросом с полученным")
def test_see_status_code_with_patch_request(send):  # Проверка статусов при отправке PATCH запроса
    url = base_url + resource + str(send)
    r = requests.patch(url)
    assert_that(r.status_code, equal_to(send))


@pytest.mark.parametrize(
    "send", status_code)
@allure.title("Сравниваем отправленный статус код {send} DELETE запросом с полученным")
def test_see_status_code_with_delete_request(send):  # Проверка статусов при отправке DELETE запроса
    url = base_url + resource + str(send)
    r = requests.delete(url)
    assert_that(r.status_code, equal_to(send))


@pytest.mark.parametrize(
    "method,send",
    [('GET', 404),
     ('POST', 501),
     ('PUT', 201),
     ('PATCH', 502),
     ('DELETE', 202)])
@allure.title("Проверяем тип контента при отправлении стутс кода {send} {method} запросом")
def test_see_type_content_with_request(method, send):  # Проверка типа контента при отправке GET, POST, PUT, PATH,
    # DELETE запросов
    url = base_url + resource + str(send)
    r = requests.request(method, url)
    content_type = r.headers["Content-Type"]
    assert_that(r.headers["Content-Type"], equal_to('text/html; charset=utf-8'))


@pytest.mark.parametrize(
    "method", ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
@allure.title("Проверяем ответ при отправке {method} запросом текста, вместо числа статус кода")
def test_see_status_code_with_text_in_request(method):  # Проверка ответа при отправке GET, POST, PUT, PATH, DELETE
    # запросов с текстом, вместо кода
    url = base_url + resource + "test"
    r = requests.delete(url)
    assert_that(r.text, equal_to("Invalid status code"))


@pytest.mark.parametrize(
    "method", ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
@allure.title("Проверяем ответ при отправке {method} запросом пустых запросов, вместо статус кода")
def test_see_status_code_with_empty_in_request(method):  # Проверка ответа при отправке GET, POST, PUT, PATH, DELETE
    # пустых запросов, вместо кода
    url = base_url + resource
    r = requests.delete(url)
    assert_that(r.status_code, equal_to(404))


@pytest.mark.parametrize(
    "method, send",
    [('GET', 1),
     ('POST', 1000),
     ('PUT', 1111),
     ('PATCH', 9),
     ('DELETE', 99)
     ])
@allure.title("Проверяем ответ при отправке {method} запросом несуществующего статус-кода, состоящего из цифр")
def test_see_status_code_with_impossible_code_in_request(method, send):  # Проверка ответа при отправке GET, POST, PUT,
    # PATH,  DELETE несуществующего запроса, вместо кода
    url = base_url + resource + str(send)
    r = requests.delete(url)
    assert_that(r.status_code, equal_to(502))

